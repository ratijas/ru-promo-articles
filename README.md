# KDE.ru Promo Articles

This repository is used for collaborative translation and editing of KDE-related news, blog posts, and articles.

## How-to

To generate a Markdown document for translation, use [ru-promo-scripts](https://invent.kde.org/ilyabizyaev/ru-promo-scripts).

For translation, consider using an offline text editor that supports Markdown linting and spell checking. For example, [Visual Studio Code](https://code.visualstudio.com) with [LanguageTool integration](https://marketplace.visualstudio.com/items?itemName=davidlday.languagetool-linter) and a [Markdown linter](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint).

Make sure your text follows the [guidelines](https://community.kde.org/RU/Контрольный_список_для_переводов_статей).

**To submit your changes:**

* Log in to Invent with your [Identity](https://identity.kde.org) account. If you don't have one, get it and make sure to specify your name in English during registration.
* If you have push access, make changes directly using the “Edit” button in GitLab above the target file: just replace its contents with your work.
* Otherwise, open a merge request.

When your translation gets edited, you can view editor's changes by clicking the commit name above the viewed file. The fewer changes are required, the sooner your work is published 😉

## Useful links

### Tools

* [KDE Translation Search](https://l10n.kde.org/dictionary/search-translations.php)
* [LanguageTool](https://languagetool.org/ru)
* [Yoficator](https://e2yo.github.io/eyo-browser)

### Resources

* [Published articles (VK)](https://vk.com/@kde_ru)
* [Russian front end & VCS dictionaries](https://github.com/web-standards-ru/dictionary)
