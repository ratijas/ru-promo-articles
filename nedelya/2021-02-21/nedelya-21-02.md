# This week in KDE: Plasma 5.21 is finally here!

> 14–21 февраля, основное — прим. переводчика

This week we released [Plasma 5.21](https://kde.org/announcements/plasma/5/5.21.0/) and have been hard at work fixing the bugs you fine folks have found with it. 🙂 Frankly I’m pretty exhausted after a long week so let’s just get right into it:

## New Features

Kate [now lets you perform basic git operations from within the app, such as viewing diffs, staging, committing](https://invent.kde.org/utilities/kate/-/merge_requests/255), and [stashing](https://invent.kde.org/utilities/kate/-/merge_requests/272)! (Waqar Ahmed, Kate 21.04)

Vertical and horizontal maximization [now works in the Plasma Wayland session](https://bugs.kde.org/show_bug.cgi?id=407793) (Влад Загородний, Plasma 5.22)

## Bugfixes & Performance Improvements

Ark [no longer asks for confirmation twice when you update a file in an archive](https://bugs.kde.org/show_bug.cgi?id=382606) (Jan Paul Batrina, Ark 21.04)

Keyboard repeat [is no longer disabled](https://bugs.kde.org/show_bug.cgi?id=431923) (Jan Blackquill, Plasma 5.21.1, and most distros have already rolled it out early)

The Task Manager [once again allows you to run non-distro-provided executable programs you’ve pinned to it](https://bugs.kde.org/show_bug.cgi?id=433148) (Alexander Lohnau, Plasma 5.21.1)

The Plasma Wayland session [no longer crashes on login when using an Nvidia Optimus laptop](https://bugs.kde.org/show_bug.cgi?id=433145) (Xaver Hugl, Plasma 5.21.1)

Trying to log out [no longer sometimes just fails](https://bugs.kde.org/show_bug.cgi?id=432460) (David Edmundson, Plasma 5.21.1)

Clicking on a screenshot in Discover [now displays the correct one](https://bugs.kde.org/show_bug.cgi?id=433123) (Aleix Pol Gonzalez, Plasma 5.21.1)

The Kickoff Application Launcher [now works with a stylus](https://bugs.kde.org/show_bug.cgi?id=433026) (Mikel Johnson, Plasma 5.21.1)

The back arrow in System Settings’ sidebar header [no longer looks wrong when using a non-Breeze icon theme](https://bugs.kde.org/show_bug.cgi?id=433062) (Nate Graham, Plasma 5.21.1)

Syncing your user settings to the SDDM login screen [now actually causes non-default font settings to take effect there, at least when using SDDM 0.19 or later](https://bugs.kde.org/show_bug.cgi?id=432930) (Filip Fila and David Redondo, Plasma 5.21.1)

Your wobbly windows [once again wobble correctly](https://bugs.kde.org/show_bug.cgi?id=433187) (Влад Загородний, Plasma 5.21.1)

Plasma [no longer crashes when you install a Windows app using Wine](https://bugs.kde.org/show_bug.cgi?id=431334) (Kai Uwe Broulik, Frameworks 5.80)

The buttons on either side of KRunner’s text field [no longer display glitchy tooltips when no search results are visible](https://bugs.kde.org/show_bug.cgi?id=433243) (Noah Davis, Frameworks 5.80)

Grid views in System Settings and elsewhere [no longer display irritating misalignment for adjacent items where one has a subtitle and the other does not](https://invent.kde.org/frameworks/kdeclarative/-/merge_requests/52#note_185751) (Nate Graham, Frameworks 5.80):

![](https://pointieststick.files.wordpress.com/2021/02/aligned-now.png?w=942)

## User Interface Improvements

Discover [no longer truncates reviews shown on app pages](https://bugs.kde.org/show_bug.cgi?id=433078) (Nate Graham, Plasma 5.21.1)

The System Settings Boot Splash page [now uses the nicer new-style Get New [Thing] dialog](https://invent.kde.org/plasma/plymouth-kcm/-/merge_requests/5) (Alexander Lohnau, Plasma 5.22)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку фонду](https://kde.org/community/donations/) [KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Илья Андреев](https://vk.com/ilyandl)  
_Источник:_ https://pointieststick.com/2021/02/19/this-week-in-kde-plasma-5-21-is-finally-here/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: get new [thing] → «Загрузить [что-то]» -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
