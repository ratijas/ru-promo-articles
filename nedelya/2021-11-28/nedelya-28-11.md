# На этой неделе в KDE: Fixing a bunch of annoying bugs

> 21–28 ноября, основное — прим. переводчика

This was a major bug squashing week, with quite a lot of annoying issues fixed — some recent regressions, and many longstanding issues as well.

On the subject of bugs and recent regressions, I’m starting to think from a higher level about how we can prevent them. KDE has largely conquered our historical issues of excessive resource consumption and visual ugliness, and our next major challenge is reliability. One idea I’m toying with is starting an initiative to focus on the «15 minute bugs» — those embarrassing issues that can easily be found within just a few minutes of using the system normally. [Here is a preliminary list of these issues in Plasma](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=1939484&order=dupecount%20DESC%2Cbug_severity%2Cchangeddate%20DESC%2Cbug_status%2Cpriority%2Cassigned_to%2Cbug_id&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced). I would encourage any experienced developers to try to focus on them! The impact will be very high.

## Wayland

In the Plasma Wayland session, [dragging a file or folder from a Folder View popup into its parent folder no longer causes Plasma to crash](https://bugs.kde.org/show_bug.cgi?id=399864) (Marco Martin, Plasma 5.24)

In the Wayland session, [when using a stylus, it’s now possible to activate other window from their titlebars and also just interact with titlebars more generally](https://bugs.kde.org/show_bug.cgi?id=432104) (Fushan Wen, Plasma 5.24)

In the Wayland session, [the Clipboard applet now shows entries for images added to the clipboard using the `wl-copy` command-line program](https://bugs.kde.org/show_bug.cgi?id=442923) (Méven Car, Plasma 5.24)

## Исправления ошибок и улучшения производительности

Creating archives using Ark’s main UI [once again works](https://bugs.kde.org/show_bug.cgi?id=445610) (Kai Uwe Broulik, Ark 21.12)

Okular’s zoom buttons [now always enable and disable themselves at the correct times, in particular when a new document is opened](https://bugs.kde.org/show_bug.cgi?id=440173) (Albert Astals Cid, Okular 21.12)

Ark [can now handle archives whose files internally use absolute paths, rather than relative paths](https://invent.kde.org/utilities/ark/-/merge_requests/30) (Kai Uwe Broulik, Ark 22.04)

Touch scrolling in Konsole [now works properly](https://bugs.kde.org/show_bug.cgi?id=437553) (Henry Heino, Konsole 22.04)

Fixed [a common crash in the System Tray](https://bugs.kde.org/show_bug.cgi?id=443961) (Fushan Wen, Plasma 5.23.4)

Fixed [a common crash in Discover when using it to manage Flatpak apps](https://bugs.kde.org/show_bug.cgi?id=443745) (Aleix Pol Gonzalez, Plasma 5.23.4)

The logout screen [once again has a blurred background](https://bugs.kde.org/show_bug.cgi?id=444899) and [animates when appearing and disappearing](https://bugs.kde.org/show_bug.cgi?id=444898) (David Edmundson, Plasma 5.23.4)

Changing various settings in System Settings [no longer causes a flickering effect behind Plasma panels](https://invent.kde.org/plasma/kwin/-/merge_requests/1672) (Влад Загородний, Plasma 5.24)

## Улучшения пользовательского интерфейса

[Kate has been replaced with KWrite in the default set of favorite apps](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/704), since it’s a bit more user-friendly and less programmer-centric (Nate Graham, Plasma 5.24)

Discover’s somewhat confusing checkbox on the bottom of the Updates page [has been transformed into a couple of buttons and a label which should be clearer](https://invent.kde.org/plasma/discover/-/merge_requests/207), and [it also doesn’t say the word «Updates» quite so many times on that page anymore](https://invent.kde.org/plasma/discover/-/merge_requests/206) (Nate Graham, Plasma 5.24):

![0](https://pointieststick.files.wordpress.com/2021/11/updates-page-now.png)

When using PipeWire and streaming audio from one device to another, [the audio stream now shows the name of the remote device in Plasma’s Audio Volume applet](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/96) (Nicolas Fella, Plasma 5.24)

The Properties window for files [now displays which app will open the file](https://invent.kde.org/frameworks/kio/-/merge_requests/636) (Kai Uwe Broulik, Frameworks 5.89):

![1](https://pointieststick.files.wordpress.com/2021/11/screenshot_20211122_114332.png)

The folder icon selection dialog [now pre-selects the currently used icon for easier visualization and keyboard navigation](https://invent.kde.org/frameworks/kiconthemes/-/merge_requests/48) (Kai Uwe Broulik, Frameworks 5.89)

Those little transient messages that sometimes appear at the bottom of the windows of Kirigami-based apps (which are nonsensically called «Toasts» in Android land) [now have easier-to-read text](https://bugs.kde.org/show_bug.cgi?id=440390) (Felipe Kinoshita, Frameworks 5.89)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Илья Андреев](https://vk.com/ilyandl)  
_Источник:_ https://pointieststick.com/2021/11/26/this-week-in-kde-fixing-a-bunch-of-annoying-bugs/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
